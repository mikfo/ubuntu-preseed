# Creating a custom Ubuntu install image

Get the iso

`wget -c http://se.releases.ubuntu.com/19.10/ubuntu-19.10-desktop-amd64.iso`

mount the iso

`sudo mkdir -p /media/mount/iso`

`sudo mount -o loop ubuntu-19.10-desktop-amd64.iso  /media/mount/iso`

copy iso to disk and make it readable
```
sudo -i
mkdir -p /opt/ubuntuiso
cp -rT /media/mount/iso /opt/ubuntuiso
cd /opt/ubuntuiso
chmod -R 755 .
```

edit boot options

```
vim isolinux/txt.cfg
```


```
default autoinstall
label autoinstall
  menu label ^Automatically install Ubuntu
  kernel /casper/vmlinuz
  append file=/cdrom/preseed/ubuntu-19.10.seed auto=true priority=critical debian-installer/locale=sv_SE keyboard-configuration/layoutcode=se ubiquity/reboot=true languagechooser/language-name=English countrychooser/shortlist=SE localechooser/supported-locales=sv_SE.UTF-8 boot=casper automatic-ubiquity initrd=/casper/initrd quiet splash noprompt noshell --
#label live
#  menu label ^Try Ubuntu without installing
#  kernel /casper/vmlinuz
#  append  file=/cdrom/preseed/ubuntu.seed initrd=/casper/initrd quiet splash ---
#label live-nomodeset
#  menu label ^Try Ubuntu without installing (safe graphics)
#  kernel /casper/vmlinuz
#  append  file=/cdrom/preseed/ubuntu.seed initrd=/casper/initrd quiet splash nomodeset ---
#label live-install
#  menu label ^Install Ubuntu
#  kernel /casper/vmlinuz
#  append  file=/cdrom/preseed/ubuntu.seed only-ubiquity initrd=/casper/initrd quiet splash ---
#label live-install-nomodeset
#  menu label ^Install Ubuntu (safe graphics)
#  kernel /casper/vmlinuz
#  append  file=/cdrom/preseed/ubuntu.seed only-ubiquity initrd=/casper/initrd quiet splash nomodeset ---
#label check
#  menu label ^Check disc for defects
#  kernel /casper/vmlinuz
#  append  integrity-check initrd=/casper/initrd quiet splash ---
#label memtest
#  menu label Test ^memory
#  kernel /install/mt86plus
#label hd
#  menu label ^Boot from first hard disk
#  localboot 0x80
```

copy seed file 
```
cp ~/ubuntu-19.10.seed preseed/
chmod ugo+x preseed/ubuntu-19.10.seed
```

Add offline packages by downloading from ubuntu 19.10
```
mkdir pool/extras
cd pool/extras
apt-get download $(apt-cache depends --recurse --no-recommends --no-suggests \
  --no-conflicts --no-breaks --no-replaces --no-enhances \
  --no-pre-depends cmake curl emacs ninja-build vim git | grep "^\w")
cd ../..
mkdir -p dists/stable/extras/binary-i386/Packages
apt-ftparchive packages ./pool/extras/ | tee dists/stable/extras/binary-i386/Packages
```

Create iso
`mkisofs -D -r -V "$IMAGE_NAME" -cache-inodes -J -l -b isolinux/isolinux.bin  -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o ~ubuntu-19.10-preseed.iso .`
